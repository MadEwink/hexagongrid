package body AxialCoordinates is
	function "+"(Left,Right : Coordinates) return Coordinates is (q => Left.q + Right.q, r => Left.r + Right.r);
	function "-"(Right : Coordinates) return Coordinates is (q => -Right.q, r => -Right.r);
	function "-"(Left,Right : Coordinates) return Coordinates is (Left + (-Right));
end AxialCoordinates;
