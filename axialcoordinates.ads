package AxialCoordinates is
	type Coordinates is
		record
			q,r : Integer;
		end record;
	function "+"(Left,Right : Coordinates) return Coordinates;
	function "-"(Right : Coordinates) return Coordinates;
	function "-"(Left,Right : Coordinates) return Coordinates;
end AxialCoordinates;
