package body CubeCoordinates is
	function "+"(Left,Right : Coordinates) return Coordinates is (x => Left.x + Right.x, y => Left.y + Right.y, z => Left.z + Right.z);
	function "-"(Right : Coordinates) return Coordinates is (x => -Right.x, y => -Right.y, z => -Right.z);
	function "-"(Left,Right : Coordinates) return Coordinates is (Left + (-Right));
end CubeCoordinates;
