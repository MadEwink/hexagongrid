package CubeCoordinates is
	type Coordinates is
		record
			x,y,z : Integer;
		end record;
	function "+"(Left,Right : Coordinates) return Coordinates;
	function "-"(Right : Coordinates) return Coordinates;
	function "-"(Left,Right : Coordinates) return Coordinates;
end CubeCoordinates;
