sources = $(wildcard *.adb)
ada_lib_info = $(patsubst %.adb, %.ali, $(sources))
object = $(patsubst %.adb, %.o, $(sources))
main = test

all : $(ada_lib_info)
	gnat bind $(main).ali
	gnat link $(main).ali
	make clean
	mv $(main) bin/

%.ali : %.adb
	gnat compile $^

clean :
	rm -f *.ali *.o

