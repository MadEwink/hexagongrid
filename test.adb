with CubeCoordinates; use CubeCoordinates;
with AxialCoordinates; use AxialCoordinates;
with ADA.Text_IO;

procedure Test is
	c : CubeCoordinates.Coordinates := (x => 1, y => 2, z => 3);
	a : AxialCoordinates.Coordinates := (q => 1, r => 2);
begin
	ADA.Text_IO.Put_Line("a : (" & a.q'Image & "," & a.r'Image & ")");
	a := a+a;
	a := a-a;
	c := c+c;
	ADA.Text_IO.Put_Line("c : (" & c.x'Image & "," & c.y'Image & "," & c.z'Image & ")");
	ADA.Text_IO.Put_Line("a : (" & a.q'Image & "," & a.r'Image & ")");
end Test;
